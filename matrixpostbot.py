from datetime import datetime
import random

import click
from flask import Flask, make_response, render_template, request
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from flask_sqlalchemy import SQLAlchemy
from matrix_client.client import MatrixClient
import yaml
random = random.SystemRandom()

app = Flask(__name__)
limiter = Limiter(
    app,
    key_func=get_remote_address,
    default_limits=['10 per minute'],
)

with open('config.yaml') as stream:
    try:
        yamlconfig = yaml.safe_load(stream)
        app.config['SQLALCHEMY_DATABASE_URI'] = yamlconfig['DATABASE']['SQLALCHEMY_DATABASE_URI']
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = yamlconfig['DATABASE']['SQLALCHEMY_TRACK_MODIFICATIONS']
    except yaml.YAMLError as exc:
        print(exc)

db = SQLAlchemy(app)


class APIKeys(db.Model):
    """
    Table that stores API Keys and room associations
    """
    __tablename__ = 'apikeys'
    id = db.Column(db.Integer, primary_key=True)
    apikey = db.Column(db.Text, unique=True)
    reference = db.Column(db.Text, nullable=True)
    owner = db.Column(db.Text, nullable=True)


class Rooms(db.Model):
    """
    Table that stores the room information
    """
    __tablename__ = 'rooms'
    id = db.Column(db.Integer, primary_key=True)
    roomname = db.Column(db.Text, unique=True)


class Mappings(db.Model):
    """
    Table that deals with the many to many relationship between Keys and Rooms
    """
    __tablename__ = 'mappings'
    id = db.Column(db.Integer, primary_key=True)
    apikey_id = db.Column(db.Integer, db.ForeignKey('apikeys.id'))
    room_id = db.Column(db.Integer, db.ForeignKey('rooms.id'))


def generate_key(length=50):
    """
    Function that throws some characters together in a better way than letting people pick something
    """
    chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*(-_=+)'
    newkey = ''.join(random.choice(chars) for i in range(length))
    return newkey


@app.before_first_request
def before_first_request():
    """
    We need to create the database and connect to Matrix before we can respond to requests
    """
    try:
        db.create_all()
        db.session.commit()
    except Exception:
        print('Unable to create database. May already exist or be misconfigured')


try:
    client = MatrixClient(yamlconfig['MATRIX']['SERVER'])
    token = client.login_with_password(username=yamlconfig['MATRIX']['USERNAME'], password=yamlconfig['MATRIX']['PASSWORD'])
except Exception:
    print('Error! Unable to connect to Matrix')


@app.route('/', methods=['GET'])
@limiter.limit(yamlconfig['API']['RATELIMIT'])
def index():
    return render_template('index.html')


@app.route('/healthcheck', methods=['GET'])
def healthcheck():
    return make_response('OK', 200)


@app.route('/api/v1/post', methods=['POST'])
@limiter.limit(yamlconfig['API']['RATELIMIT'])
def v1_post():
    try:
        apikey = request.form['apikey']
        keys = APIKeys.query.filter_by(apikey=apikey).first()
    except Exception:
        return make_response('No API Key provided', 403)

    room_name = request.form['room']
    print('Room name: {0}'.format(room_name))
    room_check = Rooms.query.filter_by(roomname=room_name).first()
    if room_check is None:
        return make_response('Room does not exist in database', 403)
    else:
        maps = Mappings.query.filter_by(apikey_id=keys.id).filter_by(room_id=room_check.id).first()
        if maps is None:
            return make_response('Access to room denied', 403)
        else:
            room = client.join_room(room_check.roomname)
            room.send_text(request.form['message'])
            print("POST request from {0} (API Key ID: {1}) triggered sending message '{2}' to room '{3}' at {4} (UTC)".format(request.remote_addr, keys.id, request.form['message'], room_check.roomname, datetime.utcnow()))
            return make_response('Message sent', 201)


@app.cli.command()
@click.option('--room', help='Name of the room the API Key should be able to send messages to')
@click.option('--reference', help='A reference, such as a request number for the API Key')
@click.option('--owner', help='The name of the owner of the API Key')
def addkey(room, reference, owner):
    """
    A CLI command to add an API Key to the Database
    """
    secretkey = generate_key()
    new_apikey = APIKeys(apikey=secretkey, reference=reference, owner=owner)
    try:
        # Check if the room already exists
        room_check = Rooms.query.filter_by(roomname=room).first()
        if room_check is None:
            new_room = Rooms(roomname=room)
            db.session.add(new_room)
        else:
            new_room.id = room_check.id
        db.session.add(new_apikey)
        db.session.commit()
        new_mapping = Mappings(apikey_id=new_apikey.id, room_id=new_room.id)
        db.session.add(new_mapping)
        db.session.commit()
        click.echo('Added an API Key for sending messages to {0} with ID {1}'.format(room, new_apikey.id))
        click.echo('The API Key is owned by "{0}" with reference "{1}"'.format(owner, reference))
        click.echo('The API Key is: {0}'.format(secretkey))
    except Exception:
        click.echo('There was an error creating the API Key. Please try again.')


@app.cli.command()
@click.option('--room', help='Name of the room the API Key should be able to send messages to')
@click.option('--apiid', help='ID of the API Key to add the room to')
def addroom(room, apiid):
    """
    Add a room to an existing API Key
    """
    try:
        # Check if the API Key already exists
        key_check = APIKeys.query.filter_by(id=apiid).first()
        if key_check is None:
            click.echo('API Key with ID {0} does not exist'.format(apiid))
        else:
            # Check if the room already exists, add it if it doesn't
            room_check = Rooms.query.filter_by(roomname=room).first()
            if room_check is None:
                new_room = Rooms(roomname=room)
                click.echo('Adding the room {0} to the database'.format(room))
                db.session.add(new_room)
                db.session.commit()
            else:
                click.echo('The room with the ID {0} exists'.format(room_check.id))
            # Check if the mapping already exists, add it if it doesn't
            map_check = Mappings.query.filter_by(apikey_id=key_check.id, room_id=room_check.id).first()
            if map_check is None:
                new_mapping = Mappings(apikey_id=key_check.id, room_id=room_check.id)
                db.session.add(new_mapping)
                db.session.commit()
                click.echo('Added access to the room {0} with the API Key {1}'.format(room, apiid))
            else:
                click.echo('Mapping already exists, nothing needs to be done')
    except Exception:
        click.echo('There was an error adding access to the API Key. Please try again.')


@app.cli.command()
def setupdb():
    """
    Setup the database
    """
    try:
        db.create_all()
        db.session.commit()
    except Exception:
        print('Unable to create database. May already exist or be misconfigured')


if __name__ == '__main__':
    app.run()
